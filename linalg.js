
function guass_eliminate(m)
{
    // Ensure a diagonal without zeroes
    for (var i = 0; i < m.length; i++)
    {
        if (m[i][i] == 0)
        {
            for (var j = 0; j < m.length; j++)
            {
                if (m[j][i] != 0)
                {
                    m[i] = add_vectors(m[i], m[j])
                }
            }
            if (m[i][i] == 0)
            {
                // Error, can't gauss eliminate since column i is zero
            }
        }
        m[i] = sub_normalize(m[i])
    }

    // Triangualize
    for (var i = 0; i < m.length - 1; i++)
    {
        for (var j = i+1; j < m.length; j++)
        {
            if (m[i][i] != 0 && m[j][i] != 0)
            {
                var ri = mul_vector_scalar(m[i], m[j][i])
                var rj = mul_vector_scalar(m[j], m[i][i])
                m[j] = subtract_vectors(rj, ri)
                m[j] = sub_normalize(m[j])
            }
        }
    }

    // Back-substitute
    for (var i = m.length - 1; i >= 0; i--)
    {
        for (var j = 0; j < i; j++)
        {
            if (m[i][i] != 0 && m[j][i] != 0)
            {
                var ri = mul_vector_scalar(m[i], m[j][i])
                var rj = mul_vector_scalar(m[j], m[i][i])
                m[j] = subtract_vectors(rj, ri)
                m[j] = sub_normalize(m[j])
            }
        }
    }

    // Normalize
    for (var i = 0; i < m.length; i++)
    {
        m[i] = mul_vector_scalar(m[i], 1/m[i][i])
    }
}

// Divide vector by the smallest non-zero vector element
function sub_normalize(v)
{
    var min = v[0]
    for (var i = 0; i < v.length; i++)
    {
        if ((Math.abs(v[i]) < Math.abs(min)) && (v[i] != 0))
        {
            min = v[i]
        }
    }
    return (min == 0) ? v.slice(0) : mul_vector_scalar(v, 1/min)
}

function mul_matrix(m1, m2)
{
    var ml = m1
    var mr = transpose(m2)
    var result = []

    for (var i = 0; i < ml.length; i++)
    {
        result.push([])
        for (var j = 0; j < mr.length; j++)
        {
            result[i].push(scalar_product(ml[i], mr[j]))
        }
    }

    return result
}

function transpose(m)
{
    var result = []
    for (var i = 0; i < m.length; i++)
    {
        for (var j = 0; j < m[i].length; j++)
        {
            if (i == 0)
            {
                result.push([])
            }
            result[j].push(m[i][j])
        }
    }
    return result
}

function mul_matrix_vec(m, v)
{
    var result = []
    for (var i = 0; i < m.length; i++)
    {
        result.push(scalar_product(m[i], v))
    }
    return result
}

function mul_vector_scalar(v, s)
{
    var result = []
    for (var i = 0; i < v.length; i++)
    {
        result.push(v[i] * s)
    }
    return result
}

function add_vectors(v, w)
{
    var n = Math.min(v.length, w.length)
    var result = []
    for (var i = 0; i < n; i++)
    {
        result.push(v[i] + w[i])
    }
    return result
}

function subtract_vectors(v, w)
{
    return add_vectors(v, mul_vector_scalar(w, -1))
}

function scalar_product(v, w)
{
    var sum = 0
    for (var i = 0; i < Math.min(v.length, w.length); i++)
    {
        sum += v[i] * w[i]
    }
    return sum
}

