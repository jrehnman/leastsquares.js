

/**
 * For a polynomial 
 * y = c_degree*x^degree + c_(degree-1)*x^(degree-1) + ... + c_0*x^0
 * and the residual from the measured value y(x0) = y0
 * r = y0 - c_degree*x0^degree - (c_degree-1)*x0^(degree-1) - ... - c_0*x^0
 * Calculate the derivative
 * d(r^2)/d(c_term)
 * such that the return value cs from this function fulfills
 * cs . (c_degree, c_degree-1, ..., c_0)' = d(r^2)/d(c_term)
 */
function derivate_polynomial_residue_squared(degree, term, x0, y0)
{
    var coefficients = [-2*y0*Math.pow(x0,term)]
    for (var i = 0; i < degree+1; i++)
    {
        var c = 2*Math.pow(x0, i + term)
        coefficients.unshift(c)
    }
    return coefficients
}

// degree: degree of polynomial to find coefficents for
// xs, ys: samples to fit polynomial againt so that y'(xs[n]) = ys[n]
function curvefit(degree, xs, ys)
{
    var n = Math.min(xs.length, ys.length)

    var coeff_matrix = []
    for (var i = 0; i < degree+1; i++)
    {
        // Initialize this equation row
        var coeffs_i = []
        for (var k = 0; k < degree+2; k++)
        {
            coeffs_i.push(0)
        }

        // Calculate coefficient matrix
        for (var j = 0; j < n; j++)
        {
            var cs = derivate_polynomial_residue_squared(degree, i, xs[j], ys[j])
            for (var k = 0; k < degree+2; k++)
            {
                coeffs_i[k] += cs[k]
            }
        }

        // Store row of equation coefficients in matrix
        coeff_matrix.push(coeffs_i)
    }

    // Solve equation system
    guass_eliminate(coeff_matrix)

    // Extract fitted values
    var coefficients = []
    for (var i = 0; i < coeff_matrix.length; i++)
    {
        coefficients.push(-coeff_matrix[i][degree+1])
    }
    return coefficients
}



